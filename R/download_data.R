# this script is used to preprocess price data
# price data are downloaded via tiing api
# https://www.tiingo.com/
# SnP components are derived fro IShares website
# https://www.ishares.com/it/investitore-privato/it/prodotti/253743/ishares-sp-500-b-ucits-etf-acc-fund
# in the period 2007--2019
# and then fixed in their previous historical evolution through wikipedia
# https://en.wikipedia.org/wiki/List_of_S%26P_500_companies

# unfortunately the download process is not here automatized,
# neither the Ishares componenets
# nor the tiingo prices
# but it easily doable

# after the download many minor fixing had to be done
# e.g. for actually matching the downloaded price with the SnP components
# (e.g. merging of companies throuhg time, name changes, and so forth)


#### ISHARES ============================================================= #####

# list of snp500 components
# taken from 
# https://www.ishares.com/it/investitore-privato/it/prodotti/253743/ishares-sp-500-b-ucits-etf-acc-fund
# retrieved components at 06 and 12 of each year, spanning 2006-12-29 -- 2019-06-28
ishares <- list.files(file.path(data_path, './Ishares/'), full.names = T)


is <- foreach(file = ishares, .combine = rbind) %dopar% {
  con <- file(file,"r")
  first_line <- readLines(con, n=1)
  close(con)
  d <- as.Date(strsplit(first_line, "\"")[[1]][2], format = "%d/%m/%Y")
  pp <- fread(file, skip = 2, fill = T)
  pp$date <- d
  pp
}
is <- is[`Asset Class` == "Azionario"]

# i need to complete the list down to 2004-01-01
# do it through wikipedia change list

# 2006-06-30 no change
new200606 <- is[date == min(date)]
new200606[, date := as.Date("2006-06-30")]
new200606[, c("Ponderazione (%)", "Prezzo", "Nominale", "Valore di mercato", "Valore nozionale") := "manual"]

is <- rbind(is, new200606)

# 2005-12-31 no change
new200512 <- is[date == min(date)]
new200512[, date := as.Date("2005-12-31")]
is <- rbind(is, new200512)

# 2005-06-30 enter STZ exit GLK
new200506 <- is[date == min(date) & `Ticker dell'emittente` != "STZ"]
new200506[, date := as.Date("2005-06-30")]
new200506 <- rbind(new200506,
                   data.table("Ticker dell'emittente" = "GLK",
                              Nome = "Great Lakes Chemical", 
                              "Asset Class" = "Azionario",
                              "Ponderazione (%)" = "manual", 
                              "Prezzo" = "manual", 
                              "Nominale" = "manual",
                              "Valore di mercato" = "manual",
                              "Valore nozionale" = "manual",
                              "Settore" = "manual", 
                              "ISIN" = "manual",
                              "Cambio" = "manual",
                              Paese = "manual",
                              "Valuta di mercato" = "manual",
                              date = as.Date("2005-06-30")
                   ))

is <- rbind(is, new200506)

# 2004-12-31 no change
new200412 <- is[date == min(date)]
new200412[, date := as.Date("2004-12-31")]
is <- rbind(is, new200412)


# 2004-06-30 no change
new200406 <- is[date == min(date)]
new200406[, date := as.Date("2004-06-30")]
is <- rbind(is, new200406)


# 2003-12-31 no change
new200312 <- is[date == min(date)]
new200312[, date := as.Date("2003-12-31")]
is <- rbind(is, new200312)


# 2003-06-30 change ESRX in QTRN out
new200306 <- is[date == min(date) & `Ticker dell'emittente` != "ESRX"]
new200306[, date := as.Date("2003-06-30")]
new200306 <- rbind(new200306,
                   data.table("Ticker dell'emittente" = "QTRN",
                              Nome = "Quintiles Transnational", 
                              "Asset Class" = "Azionario",
                              "Ponderazione (%)" = "manual", 
                              "Prezzo" = "manual", 
                              "Nominale" = "manual",
                              "Valore di mercato" = "manual",
                              "Valore nozionale" = "manual",
                              "Settore" = "manual", 
                              "ISIN" = "manual",
                              "Cambio" = "manual",
                              Paese = "manual",
                              "Valuta di mercato" = "manual",
                              date = as.Date("2003-06-30")
                   ))

is <- rbind(is, new200306)


# 2002-12-31 no change
new200212 <- is[date == min(date)]
new200212[, date := as.Date("2002-12-31")]
is <- rbind(is, new200212)


# 2002-06-30 no change
new200206 <- is[date == min(date)]
new200206[, date := as.Date("2002-06-30")]
is <- rbind(is, new200206)

# 2001-12-31 no change
new200112 <- is[date == min(date)]
new200112[, date := as.Date("2001-12-31")]
is <- rbind(is, new200112)

# 2001-06-30 no change
new200106 <- is[date == min(date)]
new200106[, date := as.Date("2001-06-30")]
is <- rbind(is, new200106)

# 2000-12-31 no change
new200012 <- is[date == min(date)]
new200012[, date := as.Date("2000-12-31")]
is <- rbind(is, new200012)



# 2000-06-30 in INTU, SBL, AYE, ABK, 
# out BS, OI, GRA, CCK
new200006 <- is[date == min(date) & !`Ticker dell'emittente` %in% c("INTU", "SBL", "AYE", "ABK")]
new200006[, date := as.Date("2000-06-30")]
new200006 <- rbind(new200006,
                   data.table("Ticker dell'emittente" = c("BS", "OI", "GRA", "CCK"),
                              Nome = c("Bethlehem Steel", "Owens-Illinois", "W.R. Grace", "Crown Holdings"), 
                              "Asset Class" = "Azionario",
                              "Ponderazione (%)" = "manual", 
                              "Prezzo" = "manual", 
                              "Nominale" = "manual",
                              "Valore di mercato" = "manual",
                              "Valore nozionale" = "manual",
                              "Settore" = "manual", 
                              "ISIN" = "manual",
                              "Cambio" = "manual",
                              Paese = "manual",
                              "Valuta di mercato" = "manual",
                              date = as.Date("2000-06-30")
                   ))

is <- rbind(is, new200006)


# deal with observations with no ISIN

is[ISIN == "-", ISIN := NA]
no_isin <- is[is.na(ISIN)]$`Ticker dell'emittente` %>% unique
is[`Ticker dell'emittente` %in% no_isin, ISIN := unique(na.omit(ISIN)), by = .(`Ticker dell'emittente`)]

# remove tickers that have never a isin
is <- is[!is.na(ISIN)]

# deal with ISIN = "manual"
manual_isin <- is[ISIN == "manual"]$`Ticker dell'emittente` %>% unique
is[`Ticker dell'emittente` %in% manual_isin, ISIN := unique(setdiff(ISIN, "manual")), by = .(`Ticker dell'emittente`)]
# there are still some tickers with only isin manual


# remove * from tickers (some have it)
is[, ticker := sub("[*].*$", "", `Ticker dell'emittente`)]

# c'è un ticker di General Electric: GEC, con stesso ISIN di un altro 
# ticker di General Electric: GE, solo che la serie storica ha prezzi completamente sballati.
# decidiamo di sostituire sempre GEC con GE
is[ticker == "GEC", ticker := "GE"]


# change some tickers "by hand" on the basis of the file ./data/codifica_tickers_dismessi.csv
# where we identified some "strange" tickers in ishares by name and price

dict_tickers <- fread("./data/codifica_tickers_dismessi.csv")
setnames(dict_tickers, "TICKER", "ticker")
setkey(dict_tickers, ticker)
setkey(is, ticker)
is[dict_tickers[, .(ticker, TICKER_N)], ticker := TICKER_N]

tickers_ishares <- unique(is$ticker) %>% sort

#### RIINGO =============================================================== ####
# ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ 

# this section makes use of tiingo api for r to download stock price data
# https://www.tiingo.com/
# you need to sign up and then use the provided token
# download data is not actully perfromed here,
# you need to use riingo api for tickers in tickers_ishares

require(riingo)
riingo_browse_signup()
riingo_browse_token()
#RIINGO_TOKEN <- "64f0caecfbb6fcae7fd4e962f2985a5239c68d27"
riingo_set_token(RIINGO_TOKEN)


tt <- supported_tickers()

dt <- fread("./data/prices_riingo.csv")
dt[, date := as.Date(fastPOSIXct(date))]
dt[, endDate := as.Date(fastPOSIXct(endDate))]
dt[, startDate := as.Date(fastPOSIXct(startDate))]
meta <- fread("./data/meta_riingo.csv")
meta[, endDate := as.Date(fastPOSIXct(endDate))]
meta[, startDate := as.Date(fastPOSIXct(startDate))]




dt <- dt[ticker %in% tickers_ishares] # restrict to ishares tickers

# elimino tutti i ticker che non hanno almeno 3 anni di dati
dt <- dt %>% group_by(ticker) %>% mutate(N = n()) %>% ungroup() %>%
  filter(N > 252*3) %>% 
  select(-N) %>% as.data.table

# check for anomalies

anom <- function(xx, K) {
  rtn <- na.omit(xx/shift(xx)) - 1 
  if(length(rtn) < 50) return(F)
  
  sigma <- sd(rtn)
  mu <- mean(rtn)
  cond <- any(rtn < (mu - K * sigma) | rtn > (mu + K * sigma))
  #cond <- any(abs(rtn) > K)
  if(cond) {
    return(T)
  } else return(F)
}
anomali <- dt[order(ticker, date), anom(adjClose, 15), by = .(year(date), ticker)]
#anomali <- dt[order(ticker, date), anom(adjClose, .6), by = .(ticker)]
anomali <- anomali[V1 == T]$ticker

# remove anomalous stocks

dt <- dt[!ticker %in% anomali]


##### FIX ISIN AND TICKERS ================================================ ####
# ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ # ~ 

# attach ISIN to all time series

isin <- is[, .(ticker, ISIN)] %>% unique

multi_ticker <- isin[, N := .N, by = ISIN][N > 1 & ISIN != "manual"]
multi_ticker[, new_ticker := paste(sort(ticker), collapse = "**"), by = ISIN]
dt_isin <- merge(dt, multi_ticker[, .(ticker, new_ticker)] %>% unique, by = "ticker", allow.cartesian = T)
dt_isin[, name := paste(sort(unique(name)), collapse = "**"), by = .(new_ticker)]
dt_isin[, c("startDate", "endDate") := .(min(startDate), max(endDate)), by = .(new_ticker)]
cols = setdiff(names(dt), c("ticker", "date", "name", "startDate", "endDate"))
dt_isin <- dt_isin[, lapply(.SD, function(x) mean(x, na.rm = T)), 
                   by = .(new_ticker, date, name, startDate, endDate), .SDcols = cols]
dt_rest <- dt[!ticker %in% multi_ticker$ticker]


dt <- rbind(dt_isin %>% rename(ticker = new_ticker), dt_rest)
setkey(dt, ticker, date)


# attach new ticker to ishares as well

is <- merge(is, multi_ticker[, .(ticker, new_ticker)], by = "ticker", all.x = T)
is[is.na(new_ticker), new_ticker := ticker][, ticker := NULL]
setnames(is, "new_ticker", "ticker")







#### FIX CALENDAR ========================================================= ####



# fix dates with a common calendar
# take all days without weekends
total_dates <- seq.Date(from = min(dt$date), to = max(dt$date), by = 1)
total_dates <- total_dates[!wday(total_dates, week_start = 1) %in% c(6, 7)]

dt[, c("min_t", "max_t") := .(min(date), max(date)), by = ticker]

#between min and max date we complete the obs with NA

dd <- CJ(ticker = dt$ticker, date = total_dates, unique = T)
ddt <- merge(dt, dd, all.y = T, by = c("ticker", "date"))
# fill min_t and max_t
ddt[, min_t := na.omit(min_t) %>% unique, by = ticker]
ddt[, max_t := na.omit(max_t) %>% unique, by = ticker]
ddt[, name := na.omit(name) %>% unique, by = ticker]
ddt <- ddt[, .SD[date >= min_t & date <= max_t], by = ticker]

ddt[order(ticker, date), adjClose := zoo::na.locf(adjClose, na.rm = F), by = .(ticker)]
ddt[order(ticker, date), adjVolume := zoo::na.locf(adjVolume, na.rm = F), by = .(ticker)]

dt <- ddt

dt[order(ticker, date), rtn := adjClose/shift(adjClose, 1) - 1, by = ticker]
dt <- dt[!is.na(rtn)]


# write the preprocessed price and components to disk
fwrite(dt, "./data/prices_preprocessed.csv", sep = ';')
fwrite(is, "./data/ishares_preprocessed.csv", sep = ';')



# correlations
require(corrplot)
require(Hmisc)
ddt <- dcast(dt[year(date) < 2016], date ~ ticker, value.var = "rtn", fill = NA)
ddt <- ddt[, !"date"] %>% data.matrix()

corre <- rcorr(ddt, na.rm = T)
corre <- cor(ddt, use = "pairwise.complete.obs")


summary(corre$r)

# correlation plot, clustered
corrplot(corre, 
         #p.mat = corre$P, sig.level = .05,
         order = 'hclust', hclust.method = "ward", 
         method = "shade", 
         tl.col = 'black', tl.cex = .1)
diag(corre) <- 0
hist(corre %>% as.vector, breaks = 100)

