# Pairs trading strategies with LSTM predictions

This project aims at analyzing various approaches to pairs trading strategies, and proposes a new indicator to be used in this respect: an LSTM prediction of the cointegration gap.

The project is entirely developed in `R`, in particular exploiting `Keras` as Neural Network development library, and `PerformanceAnalytics` for all the portfolio analysis. 

The project is organized in:

* a `main.R` which makes all the core analysis, namely:

   - the cointegration loop to identify groups of cointegrated stocks
   - the LSTM loop, with the rolling train/predict phases

* a `R/strategy.R` script, managing all the relevant statistical/economical analysis done on cointegrated stocks. Building pairs-trading portfolios with respect to different indicators and comparing them in different ways

* a `R/fun.R` script, containing all the utilities function used throughout the analysis

* other minor scripts, managing data preparation and some specific summary statistics
